//    Copyright 2020 Michael Mestnik

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

use hazel_ge::im_str;
use hazel_ge::imgui::ImStr;
use hazel_ge::reexport::glm::{self, Vec1, Vec4};
use hazel_ge::reexport::imgui;
use hazel_ge::renderer::r2d;

use rand::distributions::OpenClosed01;
use rand::{thread_rng, Rng};

mod implementation;

pub struct ExampleLayer {
    particles: implementation::Particles,
    timer: f32,
    mtype: u32,
    colors: [Vec4; 2],
    sizes: [Vec1; 2],
    rotate_rate: f32,
    translate_rate_scale: f32,
    lifetime: f32,
    add_rate: f32,
}

impl Default for ExampleLayer {
    fn default() -> Self {
        Self {
            timer: 0.,
            mtype: 1,
            particles: Default::default(),
            lifetime: 1.,
            colors: [
                glm::vec4(1.0f32, 0., 0., 1.),
                glm::vec4(0.0f32, 0., 1., 0.24),
            ],
            sizes: [glm::vec1(0.03), glm::vec1(0.01)],
            rotate_rate: 0.0000005,
            translate_rate_scale: 0.1,
            add_rate: 60.,
        }
    }
}

fn imgui_color(ui: &hazel_ge::imgui::Ui, label: &ImStr, color: &mut Vec4) {
    let color = color.as_mut_slice();
    let mut color_mut = [color[0], color[1], color[2], color[3]];
    imgui::ColorEdit::new(label, &mut color_mut).build(&ui);
    color[0] = color_mut[0];
    color[1] = color_mut[1];
    color[2] = color_mut[2];
    color[3] = color_mut[3];
}

impl hazel_ge::Layer<(), ()> for ExampleLayer {
    fn get_name(&mut self) -> String {
        "Sandbox Particle Example Layer".to_owned()
    }

    fn on_imgui_update(&mut self, ui: &hazel_ge::imgui::Ui) {
        hazel_ge::reexport::imgui::Window::new(im_str!("Particles")).build(ui, || {
            imgui_color(ui, im_str!("Begin Color"), &mut self.colors[0]);
            imgui_color(ui, im_str!("End   Color"), &mut self.colors[1]);
            imgui::Slider::new(im_str!("Lifetime"), std::ops::RangeInclusive::new(0f32, 1.))
                .build(ui, &mut self.lifetime);
            imgui::Slider::new(im_str!("Model Type"), std::ops::RangeInclusive::new(1, 2))
                .build(ui, &mut self.mtype);
            imgui::Slider::new(
                im_str!("Begin Size"),
                std::ops::RangeInclusive::new(0f32, 0.5),
            )
            .build(ui, &mut self.sizes[0][0]);
            imgui::Slider::new(
                im_str!("End   Size"),
                std::ops::RangeInclusive::new(0f32, 0.5),
            )
            .build(ui, &mut self.sizes[1][0]);
            imgui::DragFloat::new(ui, im_str!("Speed Scale"), &mut self.translate_rate_scale)
                .min(0.)
                .max(3.)
                .speed(0.001)
                .build();
            imgui::DragFloat::new(ui, im_str!("Rate"), &mut self.add_rate)
                .min(0.)
                .build();
        })
    }

    fn on_update(
        &mut self,
        api: &mut hazel_ge::AppAPI<(), ()>,
        _: &hazel_ge::EventLoopWindowTarget<()>,
        _: &mut hazel_ge::ControlFlow,
    ) {
        if api.input.get_button(0) {
            let world_pos = api.input.position;
            let window_size = api.renderer.window.inner_size();
            let world_pos = &glm::vec3(
                2. * (world_pos.x as f32 / window_size.width as f32) - 1.,
                1. - 2. * (world_pos.y as f32 / window_size.height as f32),
                0.5,
            );

            let ticks = [self.timer.floor() as usize, {
                self.timer += api.elapsed.as_secs_f32() * self.add_rate;
                self.timer.floor() as _
            }];

            if ticks[1] > ticks[0] {
                let mtype = self.mtype;
                let colors = self.colors;
                let sizes = self.sizes;
                let rotate_rate = self.rotate_rate;
                let translate_rate_scale = self.translate_rate_scale;
                let lifetime = self.lifetime;
                for particle in std::iter::repeat_with(|| implementation::Particle {
                    template: r2d::Vertex {
                        transform: glm::translate(&glm::identity(), &world_pos),
                        mtype: mtype,
                        ..Default::default()
                    },
                    sizes,
                    rotate_rate,
                    translate_rate: glm::vec2(
                        thread_rng().sample::<f32, _>(OpenClosed01)
                            * if rand::random() { 1. } else { -1. },
                        thread_rng().sample::<f32, _>(OpenClosed01)
                            * if rand::random() { 1. } else { -1. },
                    ) * translate_rate_scale,
                    colors,
                    life: implementation::util::Life(lifetime, 0.),
                    ..Default::default()
                })
                .take(ticks[1] - ticks[0])
                {
                    self.particles.0.push_back(particle.clone());
                }
            }
        }
        self.particles
            .update(api.elapsed, &mut api.renderer.renderer_2d)
            .unwrap();
    }
}
