//    Copyright 2020 Michael Mestnik

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

use std::cell::RefCell;
use std::rc::Rc;

use hazel_ge::camera::{KeyMap as CameraMapping, Layer as Camera};
use hazel_ge::imgui;
use hazel_ge::reexport::slog::{info, trace};

struct ExampleEventLayer {
    ctr: i64,
}

impl hazel_ge::Layer<(), ()> for ExampleEventLayer {
    fn get_name(&mut self) -> String {
        format!("Sandbox Example Event: ctr == {};", self.ctr).to_owned()
    }

    fn on_update(
        &mut self,
        _api: &mut hazel_ge::AppAPI<(), ()>,
        _wt: &hazel_ge::reexport::winit::event_loop::EventLoopWindowTarget<()>,
        _c: &mut hazel_ge::reexport::winit::event_loop::ControlFlow,
    ) {
        // trace!(_api.log, "{}", "on_update");
    }

    fn on_event(
        &mut self,
        api: &mut hazel_ge::AppAPI<(), ()>,
        e: &hazel_ge::event::Event<()>,
        _: &hazel_ge::EventLoopWindowTarget<()>,
        _: &mut hazel_ge::ControlFlow,
    ) -> bool {
        use hazel_ge::event::{DeviceEvent::*, Event::*, WindowEvent::*};
        match e {
            NewEvents(hazel_ge::event::StartCause::Poll) => {
                self.ctr += 1;
                if self.ctr % 1000 == 0 {
                    info!(api.log, "ExampleLayer::Update");
                    self.ctr = 0;
                };
            }
            WindowEvent {
                event: CursorMoved { .. },
                ..
            } => {}
            WindowEvent {
                event: AxisMotion { .. },
                ..
            } => {}
            WindowEvent {
                event: Resized { .. },
                ..
            } => {}
            DeviceEvent {
                event: MouseMotion { .. },
                ..
            } => {}
            DeviceEvent {
                event: Motion { .. },
                ..
            } => {}
            MainEventsCleared => {}
            RedrawEventsCleared => {}
            _ => trace!(api.log, "{}", format!("{:?}", e)),
        }
        false
    }
}

struct BeginSceneLayer(Rc<RefCell<Camera>>);

impl hazel_ge::Layer<(), ()> for BeginSceneLayer {
    fn get_name(&mut self) -> String {
        "Sandbox Begin Scene".to_owned()
    }

    fn on_update(
        &mut self,
        api: &mut hazel_ge::AppAPI<(), ()>,
        _wt: &hazel_ge::reexport::winit::event_loop::EventLoopWindowTarget<()>,
        _c: &mut hazel_ge::reexport::winit::event_loop::ControlFlow,
    ) {
        api.renderer
            .begin_scene(self.0.borrow_mut().camera.get_view_projection())
            .unwrap();
    }
}

struct EndSceneLayer;

impl hazel_ge::Layer<(), ()> for EndSceneLayer {
    fn get_name(&mut self) -> String {
        "Sandbox Begin Scene".to_owned()
    }

    fn on_update(
        &mut self,
        api: &mut hazel_ge::AppAPI<(), ()>,
        _wt: &hazel_ge::reexport::winit::event_loop::EventLoopWindowTarget<()>,
        _c: &mut hazel_ge::reexport::winit::event_loop::ControlFlow,
    ) {
        api.renderer
            .end_scene()
            .into_iter()
            .collect::<hazel_ge::renderer::base::Result<()>>()
            .unwrap();
    }
}

mod particles;
// mod s2d;
// mod s3d;

struct InitLayer(Rc<RefCell<Camera>>);

impl hazel_ge::Layer<(), ()> for InitLayer {
    fn get_name(&mut self) -> String {
        "Sandbox Initial".to_owned()
    }

    fn on_preupdate(
        &mut self,
        api: &mut hazel_ge::AppAPI<(), ()>,
        _: &hazel_ge::EventLoopWindowTarget<()>,
        _: &mut hazel_ge::ControlFlow,
    ) {
        api.layers[1]
            .iter()
            .position(|it| it.try_borrow_mut().is_err())
            .map(|e| api.layers[1].remove(e));

        let layer = Rc::new(RefCell::new(BeginSceneLayer(self.0.clone())));
        api.layers[0].push(layer);

        // let layer = Rc::new(RefCell::new(s3d::ExampleLayer::new(api, self.0.clone())));
        // api.layers[1].push(layer);

        // let layer = Rc::new(RefCell::new(s2d::ExampleLayer::new(api)));
        // api.layers[1].push(layer);

        let layer = Rc::new(RefCell::new(particles::ExampleLayer::default()));
        api.layers[1].push(layer);

        let layer = Rc::new(RefCell::new(EndSceneLayer));
        api.layers[2].push(layer);
    }
}

#[derive(Default)]
struct ImguiInterface;

impl imgui::Interface for ImguiInterface {
    fn get_name(&mut self) -> String {
        "Sandbox Imgui Interface".to_owned()
    }

    fn on_update(&mut self, _ui: &hazel_ge::imgui::Ui) {}
}

fn main() {
    use hazel_ge::event::VirtualKeyCode;

    let mut app: hazel_ge::Application<(), ()> = Default::default();
    let imgui_interface = Rc::new(RefCell::new(ImguiInterface::default()));

    let innet_size = app.window.get_window().inner_size();
    let camera = Rc::new(RefCell::new(Camera::new(
        (innet_size.width / innet_size.height) as f32,
        CameraMapping {
            left: VirtualKeyCode::A,
            down: VirtualKeyCode::S,
            right: VirtualKeyCode::D,
            up: VirtualKeyCode::W,
            clockwise: Some((VirtualKeyCode::Q, VirtualKeyCode::E)),
        },
    )));

    app.layers.push(vec![
        Rc::new(RefCell::new(ExampleEventLayer { ctr: 3 })),
        camera.clone(),
    ]);
    app.layers
        .push(vec![Rc::new(RefCell::new(InitLayer(camera)))]);
    app.layers.push(vec![]);
    app.layers
        .push(vec![Rc::new(RefCell::new(imgui::InitLayer::new(
            imgui_interface,
            3,
        )))]);
    app.run()
}
