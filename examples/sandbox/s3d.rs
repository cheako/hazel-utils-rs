//    Copyright 2020 Michael Mestnik

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

use std::cell::RefCell;
use std::collections::HashMap;
use std::mem;
use std::rc::Rc;

use memoffset::offset_of;

use hazel_ge::camera::Layer as Camera;
use hazel_ge::reexport::ash::vk::PrimitiveTopology;
use hazel_ge::reexport::glm;
use hazel_ge::renderer::{
    Format, PipelineVariableValue, ShaderStageFlags, ShaderStageInput::SpirV, ShaderStagesInput,
    VertexInputAttributeDescription,
};
use hazel_ge::{glsl_fs, glsl_vs, im_str, GLSLEmbedImpl};

use glm::Vec3;

#[derive(Clone, Debug, Copy)]
struct Vertex {
    position: [f32; 3],
    color: [f32; 4],
}

#[derive(Clone, Debug, Copy)]
struct SquareVertex {
    position: [f32; 3],
    tex_coord: [f32; 2],
}

pub struct ExampleLayer {
    camera: Rc<RefCell<Camera>>,
    vertex_array: Rc<hazel_ge::renderer::VertexArray<()>>,
    square_va: Rc<hazel_ge::renderer::VertexArray<()>>,
    texture: PipelineVariableValue<()>,
    cheako_logo_texture: PipelineVariableValue<()>,
    color: Vec3,
}

impl ExampleLayer {
    pub fn new(api: &mut hazel_ge::AppAPI<(), ()>, camera: Rc<RefCell<Camera>>) -> Self {
        let vertices = [
            Vertex {
                position: [-0.5, -0.5, 0.0],
                color: [0.8, 0.2, 0.8, 1.0],
            },
            Vertex {
                position: [0.5, -0.5, 0.0],
                color: [0.2, 0.3, 0.8, 1.0],
            },
            Vertex {
                position: [0.0, 0.5, 0.0],
                color: [0.8, 0.8, 0.2, 1.0],
            },
        ];

        let vertex_buffer = api
            .renderer
            .create_vertex_buffer(mem::size_of::<Vertex>() as _, &vertices)
            .unwrap();

        let indices = [0u32, 1, 2];
        let index_buffer = api.renderer.create_index_buffer(&indices).unwrap();

        let mut shaders = HashMap::new();
        shaders.insert(
            ShaderStageFlags::VERTEX,
            SpirV(
                glsl_vs! { r#"#version 330 core
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec3 a_Position;
layout (location = 1) in vec4 a_Color;

layout (set = 0, binding = 0) uniform UBO {
    mat4 ViewProjection;
    mat4 Transform;
} u;

layout (location = 0) 			out vec3 v_Position;
layout (location = 1) 			out vec4 v_Color;

void main() {
    v_Position = a_Position;
    v_Color = a_Color;
    gl_Position = u.ViewProjection * u.Transform * vec4(a_Position, 1.0);
}"# }
                .to_vec(),
            ),
        );
        shaders.insert(
            ShaderStageFlags::FRAGMENT,
            SpirV(
                glsl_fs! { r#"#version 330 core
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) out vec4 color;

layout (location = 0) in vec3 v_Position;
layout (location = 1) in vec4 v_Color;

void main()
{
    color = vec4(v_Position * 0.5 + 0.5, 1.0);
    color = v_Color;
}"# }
                .to_vec(),
            ),
        );

        let vertex_array = hazel_ge::renderer::VertexArrayBuilder::default()
            .add_attribute(
                "a_Position",
                vec![*VertexInputAttributeDescription::builder()
                    .format(Format::R32G32B32_SFLOAT)
                    .offset(offset_of!(Vertex, position) as u32)],
            )
            .add_attribute(
                "a_Color",
                vec![*VertexInputAttributeDescription::builder()
                    .format(Format::R32G32B32A32_SFLOAT)
                    .offset(offset_of!(Vertex, color) as u32)],
            )
            .add_vertex_buffer(vertex_buffer)
            .set_index_buffer(index_buffer)
            .add_pipeline(
                &api.renderer,
                &ShaderStagesInput::Parts("vertex_pos_color".to_owned(), shaders),
                PrimitiveTopology::TRIANGLE_LIST,
                Default::default(),
            )
            .expect("Unable to create shaders graphics pipeline")
            .build()
            .unwrap();

        let vertex_array = Rc::new(vertex_array);
        api.renderer.vertex_arrays.push(vertex_array.clone());

        let square_vertices = [
            SquareVertex {
                position: [-0.5, -0.5, 0.0],
                tex_coord: [0.0, 0.0],
            },
            SquareVertex {
                position: [0.5, -0.5, 0.0],
                tex_coord: [1.0, 0.0],
            },
            SquareVertex {
                position: [0.5, 0.5, 0.0],
                tex_coord: [1.0, 1.0],
            },
            SquareVertex {
                position: [-0.5, 0.5, 0.0],
                tex_coord: [0.0, 1.0],
            },
        ];

        let square_vb = api
            .renderer
            .create_vertex_buffer(mem::size_of::<SquareVertex>() as _, &square_vertices)
            .unwrap();

        let square_indices = [0u32, 1, 2, 2, 3, 0];
        let square_ib = api.renderer.create_index_buffer(&square_indices).unwrap();

        let mut flat_color_shaders = HashMap::new();
        flat_color_shaders.insert(
            ShaderStageFlags::VERTEX,
            SpirV(
                glsl_vs! { r#"#version 330 core
#extension GL_ARB_separate_shader_objects : enable
#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec3 a_Position;

layout (set = 0, binding = 0) uniform UBO {
    mat4 ViewProjection;
    mat4 Transform;
} u;

void main() {
    gl_Position = u.ViewProjection * u.Transform * vec4(a_Position, 1.0);
}"# }
                .to_vec(),
            ),
        );
        flat_color_shaders.insert(
            ShaderStageFlags::FRAGMENT,
            SpirV(
                glsl_fs! { r#"#version 330 core
#extension GL_ARB_separate_shader_objects : enable

layout (location = 0) out vec4 color;

layout (push_constant) uniform PUSH {
    vec3 Color;
} p;

void main()
{
    color = vec4(p.Color, 1.0f);
}"# }
                .to_vec(),
            ),
        );

        let square_va = hazel_ge::renderer::VertexArrayBuilder::default()
            .add_attribute(
                "a_Position",
                vec![*VertexInputAttributeDescription::builder()
                    .format(Format::R32G32B32_SFLOAT)
                    .offset(offset_of!(SquareVertex, position) as u32)],
            )
            .add_attribute(
                "a_TexCoord",
                vec![*VertexInputAttributeDescription::builder()
                    .format(Format::R32G32_SFLOAT)
                    .offset(offset_of!(SquareVertex, tex_coord) as u32)],
            )
            .add_vertex_buffer(square_vb)
            .set_index_buffer(square_ib)
            .add_pipeline(
                &api.renderer,
                &ShaderStagesInput::File(
                    std::path::PathBuf::new()
                        .with_file_name("assets/shaders/texture.glsl"), /* Relitive to crate. */
                ),
                PrimitiveTopology::TRIANGLE_LIST,
                Default::default(),
            ).expect("Unable to create texture graphics pipeline")
            .add_pipeline(
                &api.renderer,
                &ShaderStagesInput::Parts("flat_color".to_owned(), flat_color_shaders),            PrimitiveTopology::TRIANGLE_LIST
                    ,
                Default::default(),
            ).expect("Unable to create flat color 3D graphics pipeline")
            .build().unwrap();

        let square_va = Rc::new(square_va);
        api.renderer.vertex_arrays.push(square_va.clone());

        let texture = api
            .renderer
            .load_texture_file("assets/textures/rock_03_h2.png")
            .expect("Can't open rock_03_h2.png");
        let cheako_logo_texture = api
            .renderer
            .load_texture_file("assets/textures/cheako_logo.png")
            .expect("Can't open cheako_logo.png");

        Self {
            camera,
            vertex_array,
            square_va,
            texture,
            cheako_logo_texture,
            color: Vec3::new(0.2f32, 0.3, 0.8),
        }
    }
}

impl hazel_ge::Layer<(), ()> for ExampleLayer {
    fn get_name(&mut self) -> String {
        "Sandbox 3D Example Layer".to_owned()
    }

    fn on_imgui_update(&mut self, ui: &hazel_ge::imgui::Ui) {
        let color = self.color.as_mut_slice();
        let mut color_mut = [color[0], color[1], color[2]];
        hazel_ge::reexport::imgui::ColorEdit::new(im_str!("Square 3D Color"), &mut color_mut)
            .build(&ui);
        color[0] = color_mut[0];
        color[1] = color_mut[1];
        color[2] = color_mut[2];
    }

    fn on_update(
        &mut self,
        api: &mut hazel_ge::AppAPI<(), ()>,
        _: &hazel_ge::EventLoopWindowTarget<()>,
        _: &mut hazel_ge::ControlFlow,
    ) {
        let mut pipeline_varibles = HashMap::new();
        pipeline_varibles.insert(
            "ViewProjection".to_owned(),
            PipelineVariableValue::<()>::Mat4(
                self.camera.borrow_mut().camera.get_view_projection()
                    * glm::translate(&glm::identity(), &glm::vec3(-2.4f32, 0., 0.)),
            ),
        );
        pipeline_varibles.insert(
            "Color".to_owned(),
            PipelineVariableValue::<()>::VecF32(glm::value_ptr(&self.color).to_vec()),
        );

        let mut slice = [0f32; 16];
        slice[0] = 0.1;
        slice[5] = 0.1;
        slice[10] = 1.;
        slice[15] = 1.;
        for y in 0..20 {
            for x in 0..20 {
                slice[12] = x as f32 * 0.11;
                slice[13] = y as f32 * 0.11;
                pipeline_varibles.insert(
                    "Transform".to_owned(),
                    PipelineVariableValue::<()>::VecF32(slice.to_vec()),
                );
                api.renderer
                    .submit("flat_color", &self.square_va, &pipeline_varibles)
                    .unwrap();
            }
        }

        slice[12] = -2.;
        slice[13] = -2.;
        slice[0] = 1.5;
        slice[5] = 1.5;
        slice[10] = 1.5;
        slice[15] = 1.5;
        pipeline_varibles.insert(
            "Transform".to_owned(),
            PipelineVariableValue::<()>::VecF32(slice.to_vec()),
        );

        pipeline_varibles.insert("Texture".to_owned(), self.texture.clone());
        api.renderer
            .submit("texture", &self.square_va, &pipeline_varibles)
            .unwrap();

        pipeline_varibles.insert("Texture".to_owned(), self.cheako_logo_texture.clone());
        api.renderer
            .submit("texture", &self.square_va, &pipeline_varibles)
            .unwrap();

        slice[12] = -2.;
        slice[13] = 0.;
        slice[0] = 1.;
        slice[5] = 1.;
        slice[10] = 1.;
        slice[15] = 1.;
        pipeline_varibles.insert(
            "Transform".to_owned(),
            PipelineVariableValue::<()>::VecF32(slice.to_vec()),
        );
        api.renderer
            .submit("vertex_pos_color", &self.vertex_array, &pipeline_varibles)
            .unwrap();
    }
}
