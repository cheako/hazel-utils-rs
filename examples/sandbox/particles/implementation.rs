//    Copyright 2020 Michael Mestnik

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

use std::collections::vec_deque::VecDeque;

use hazel_ge::reexport::glm::{self, lerp, Vec1, Vec2, Vec4};
use hazel_ge::renderer::r2d;

pub mod util {
    use std::ops;

    #[derive(Clone, Debug)]
    pub struct Life<T: Clone>(pub T, pub T);

    impl Default for Life<f32> {
        fn default() -> Self {
            Self(1., 0.)
        }
    }

    impl<T: Clone + ops::AddAssign + PartialOrd> Life<T> {
        pub fn update(&mut self, v: T) -> bool {
            self.1 += v;
            self.0 > self.1
        }
    }

    impl<T: Clone + Copy + ops::Div> Life<T> {
        pub fn get_progress(&self) -> T::Output {
            self.1 / self.0
        }
    }
}

#[derive(Clone, Debug, derive_builder::Builder)]
pub struct Particle {
    pub template: r2d::Vertex,
    pub colors: [Vec4; 2],
    pub sizes: [Vec1; 2],
    pub rotate_rate: f32,
    pub translate_rate: Vec2,
    pub life: util::Life<f32>,
}

impl Default for Particle {
    fn default() -> Self {
        Self {
            template: Default::default(),
            sizes: [glm::vec1(1.), glm::vec1(0.)],
            rotate_rate: 0.,
            translate_rate: glm::vec2(0., 0.),
            colors: [glm::vec4(1., 1., 1., 1.), glm::vec4(0., 0., 0., 0.)],
            life: Default::default(),
        }
    }
}

#[derive(Clone, Debug, Default)]
pub struct Particles(pub VecDeque<Particle>);

impl Particles {
    pub fn update(
        &mut self,
        elapsed: std::time::Duration,
        renderer: &mut r2d::Renderer<()>,
    ) -> r2d::Result<()> {
        let elapsed = elapsed.as_secs_f32();

        while if let Some(particle) = self.0.front_mut() {
            !particle.life.update(elapsed)
        } else {
            return Ok(());
        } {
            self.0.pop_front();
        }

        let mut latch = false;
        self.0
            .iter_mut()
            .map(|particle| {
                if latch {
                    assert!(particle.life.update(elapsed));
                } else {
                    latch = true;
                }

                let a = particle.life.get_progress();
                particle.template.color = lerp(&particle.colors[0], &particle.colors[1], a);
                let scale = lerp(&particle.sizes[0], &particle.sizes[1], a);
                let rotate =
                    glm::vec4_to_vec2(&(particle.template.transform * glm::vec4(1., 1., 0., 0.)));
                let rotate = rotate[1].atan2(rotate[0]);
                let translation =
                    glm::vec4_to_vec2(&(particle.template.transform * glm::vec4(0., 0., 0., 1.)));
                particle.template.transform = glm::scale(
                    &glm::rotate(
                        &glm::translate(
                            &glm::identity(),
                            &glm::vec2_to_vec3(&(translation + particle.translate_rate * elapsed)),
                        ),
                        rotate + particle.rotate_rate * elapsed,
                        &glm::vec3(0., 0., 1.),
                    ),
                    &glm::vec3(scale[0], scale[0], 1.),
                );
                renderer.push_vertex(&particle.template)
            })
            .collect::<Result<Vec<_>, _>>()?;
        Ok(())
    }
}
