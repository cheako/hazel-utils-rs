//    Copyright 2020 Michael Mestnik

//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at

//        http://www.apache.org/licenses/LICENSE-2.0

//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

use hazel_ge::im_str;
use hazel_ge::reexport::glm::{self, Vec4};

pub struct ExampleLayer {
    color: Vec4,
}

impl ExampleLayer {
    pub fn new(api: &mut hazel_ge::AppAPI<(), ()>) -> Self {
        api.renderer.renderer_2d.set_images(
            [
                api.renderer
                    .load_texture_file("assets/textures/rock_03_h2.png")
                    .expect("Can't open rock_03_h2.png")
                    .get_image_view()
                    .clone(),
                api.renderer
                    .load_texture_file("assets/textures/cheako_logo.png")
                    .expect("Can't open cheako_logo.png")
                    .get_image_view()
                    .clone(),
            ]
            .to_vec(),
        );

        Self {
            color: Vec4::new(0.2f32, 0.3, 0.8, 1.),
        }
    }
}

impl hazel_ge::Layer<(), ()> for ExampleLayer {
    fn get_name(&mut self) -> String {
        "Sandbox 2D Example Layer".to_owned()
    }

    fn on_imgui_update(&mut self, ui: &hazel_ge::imgui::Ui) {
        let color = self.color.as_mut_slice();
        let mut color_mut = [color[0], color[1], color[2], color[3]];
        hazel_ge::reexport::imgui::ColorEdit::new(im_str!("Square 2D Color"), &mut color_mut)
            .build(&ui);
        color[0] = color_mut[0];
        color[1] = color_mut[1];
        color[2] = color_mut[2];
        color[3] = color_mut[3];
    }

    fn on_update(
        &mut self,
        api: &mut hazel_ge::AppAPI<(), ()>,
        _: &hazel_ge::EventLoopWindowTarget<()>,
        _: &mut hazel_ge::ControlFlow,
    ) {
        {
            let renderer = &mut api.renderer.renderer_2d;
            use hazel_ge::renderer::r2d::Component::*;
            renderer
                .draw(vec![
                    POSITION(glm::vec2(-2f32, -2.)),
                    SIZE(glm::vec2(1.5f32, 1.5)),
                    IMAGE(0),
                ])
                .unwrap();

            renderer
                .draw(vec![
                    HEX,
                    POSITION(glm::vec2(-0.5f32, -2.)),
                    SIZE(glm::vec2(1.5f32, 1.5)),
                    IMAGE(0),
                ])
                .unwrap();
        }
        for y in 0..20 {
            for x in 0..20 {
                let renderer = &mut api.renderer.renderer_2d;
                use hazel_ge::renderer::r2d::Component::*;
                if (x % 2 == 0) ^ (y % 2 == 0) {
                    renderer
                        .draw(vec![
                            POSITION(glm::vec2(x as f32 * 0.11, y as f32 * 0.11)),
                            SIZE(glm::vec2(0.1f32, 0.1)),
                            COLOR(self.color),
                        ])
                        .unwrap();
                } else {
                    renderer
                        .draw(vec![
                            HEX,
                            POSITION(glm::vec2(x as f32 * 0.11, y as f32 * 0.11)),
                            SIZE(glm::vec2(0.1f32, 0.1)),
                            COLOR(self.color),
                        ])
                        .unwrap();
                };
            }
        }
        {
            let renderer = &mut api.renderer.renderer_2d;
            use hazel_ge::renderer::r2d::Component::*;
            renderer
                .draw(vec![
                    POSITION(glm::vec2(-2f32, -2.)),
                    SIZE(glm::vec2(1.5f32, 1.5)),
                    // https://www.h-schmidt.net/FloatConverter/IEEE754.html
                    Z(0.9999999),
                    COLOR(glm::vec4(0.4f32, 1., 1., 1.)),
                    IMAGE(1),
                ])
                .unwrap();

            renderer
                .draw(vec![
                    HEX,
                    POSITION(glm::vec2(-0.5f32, -2.)),
                    SIZE(glm::vec2(1.5f32, 1.5)),
                    Z(0.9999999),
                    COLOR(glm::vec4(0.4f32, 1., 1., 1.)),
                    IMAGE(1),
                ])
                .unwrap();
        }
    }
}
